package com.example.dataroom.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dataroom.model.Contact

@Dao
interface ContactDAO {
    @Insert
    fun insert(contact: Contact): Long

    /*@Update
    fun update(contact: Contact)

    @Delete
    fun delete(contact: Contact)*/

    @Query("SELECT * FROM ${Contact.TABLE_NAME} ORDER BY lastName, firstName")
    fun getContacts(): LiveData<List<Contact>>
}