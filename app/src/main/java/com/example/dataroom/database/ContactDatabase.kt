package com.example.dataroom.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.dataroom.model.Contact
import com.example.dataroom.model.Phone

@Database(entities = [Contact::class, Phone::class], version = 1)
abstract class ContactDatabase: RoomDatabase() {
    abstract fun contactDAO(): ContactDAO


    companion object {

        @Volatile
        private var INSTANCE: ContactDatabase? = null

        fun getInstance(context: Context): ContactDatabase? {
            INSTANCE ?: synchronized(this) {
                INSTANCE = Room.databaseBuilder(context, ContactDatabase::class.java, "ContactDB").build()
            }
            return INSTANCE
        }
    }
}