package com.example.dataroom.database

import android.app.Application
import android.os.AsyncTask
import com.example.dataroom.model.Contact

class ContactRepository(application: Application) {
    private val contactDAO: ContactDAO? = ContactDatabase.getInstance(application)?.contactDAO()

    fun insert(contact: Contact): Long = if (contactDAO != null) InsertAsynckTask(contactDAO).execute(contact).get() else 0


    private class InsertAsynckTask(private val contactDAO: ContactDAO): AsyncTask<Contact, Void, Long>() {
        override fun doInBackground(vararg params: Contact?): Long {
            for (contact in params) {
                return if (contact != null) contactDAO.insert(contact)
                else  0
            }
            return 0
        }
    }
}