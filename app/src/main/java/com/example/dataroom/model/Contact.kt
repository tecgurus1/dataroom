package com.example.dataroom.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity
data class Contact(@ColumnInfo(name = "firstName") @NotNull var firstName: String,
                   @ColumnInfo(name = "lastName") @NotNull var lastName: String,
                   @ColumnInfo(name = "email") @NotNull var email: String) {

    companion object {
        const val TABLE_NAME = "Contact"
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @NotNull
    var id = 0

}