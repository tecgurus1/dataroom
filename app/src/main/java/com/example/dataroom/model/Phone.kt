package com.example.dataroom.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity
data class Phone(@ColumnInfo(name = "cellphone") @NotNull var cellphone: Long,
                 @ColumnInfo(name = "phone") var phone: Long,
                 @ColumnInfo(name = "type") @NotNull var type: String) {

    companion object {
        const val TABLE_NAME = "Phone"
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @NotNull
    var id = 0

    @ForeignKey(entity = Contact::class, parentColumns = ["id"], childColumns = ["idContact"])
    @ColumnInfo(name = "idContact")
    @NotNull
    private var idContact = 0

}